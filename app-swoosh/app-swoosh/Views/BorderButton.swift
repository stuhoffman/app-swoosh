//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Stuart Hoffman on 3/20/19.
//  Copyright © 2019 Stuart Hoffman. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.white.cgColor
    }

}
